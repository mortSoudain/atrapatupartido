import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

//Componentes para enrutar aplicacion
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import App from '../../ui/App.jsx';

export default class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
          <Switch>
            <Route path='/' component={App} />
          </Switch>
      </BrowserRouter>
    );
  }
}
