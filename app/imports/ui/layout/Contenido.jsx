import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'

//Componentes para enrutar aplicacion
import { withRouter, Switch, Route } from 'react-router-dom';

// "Middleware" entre meteor y react
import { withTracker } from 'meteor/react-meteor-data';

//Contenido que se carga dinamicamente
import Home from './contenido/Home.jsx';
import IniciarSesion from './contenido/Usuarios/IniciarSesion.jsx';
import NotFound from './contenido/NotFound.jsx';
import MiPerfil from './contenido/Usuarios/MiPerfil.jsx';
import UsuariosColeccion from './contenido/Usuarios/UsuariosColeccion.jsx';

import CrearPartidoConcrete from './contenido/CrearPartidoConcrete.jsx';

export class Contenido extends Component {

  render() {

    var loggedInOrNotContent;

    if (this.props.usuarioActual) {
      loggedInOrNotContent =
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/miperfil' component={MiPerfil} />
          <Route path='/usuarios' component={UsuariosColeccion} />
          <Route path='/match' component={CrearPartidoConcrete} />
          <Route component={NotFound} />
        </Switch>;
    } else {
      loggedInOrNotContent =
        <Switch>
          <Route exact path='/' component={IniciarSesion} />
          <Route component={NotFound} />
        </Switch>;
    }

    return (<div>{loggedInOrNotContent}</div>);
  }
}

export default withTracker(() => {
  return {
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
})(Contenido);
