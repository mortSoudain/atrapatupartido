import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import speechSynthesis from 'speech-synthesis';

// "Middleware" entre meteor y react
import { withTracker } from 'meteor/react-meteor-data';

//Ant Design
import { notification, Icon } from 'antd';

export class Notificaciones extends Component {

  componentDidUpdate(prevProps,nextProps){

    // Si no hay usuario conectado
    if(!this.props.usuarioActual){
      notification.open({
        key: 'notificacion:inicial',
        message: 'Información Importante',
        description:'Nombre de Usuario: Administrador Contraseña: admin001',
        placement:'bottomRight',
        duration: 0,
        icon: <Icon type="info-circle" style={{ color: '#0e77ca' }} />,
      });
    } else {

      //Si es que hay usuario, y se acaba de conectar(previamente era falso)
      if(!prevProps.usuarioActual){
        notification.close('notificacion:inicial');
        speechSynthesis("Bienvenido a SmartControl", "es-MX");
      }
    }
  }

  render() {
    return (
      <div></div>
    );
  }

}

export default withTracker(() => {

    //Se subscribe a la coleccion de eventos
  Meteor.subscribe('eventos');

  return {
    usuarioActual: Meteor.user(),
  };
})(Notificaciones);
