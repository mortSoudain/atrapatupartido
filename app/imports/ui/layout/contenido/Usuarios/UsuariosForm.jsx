import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Input, Modal, notification } from 'antd';
const FormItem = Form.Item;

export default class UsuariosForm extends Component {

  constructor(props){
    super(props);
    this.state = {
      confirmDirty: false
    };
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="vertical">
        <FormItem
        label={(
          <span>
            Nombre de Usuario
          </span>
        )}
        hasFeedback
      >
        {getFieldDecorator('username', {
          rules: [{ required: true, message: 'Por favor ingresa un username!', whitespace: true }],
        })(
          <Input />
        )}
      </FormItem>

        <FormItem
        label="E-mail"
        hasFeedback
      >
        {getFieldDecorator('email', {
          rules: [{
            type: 'email', message: 'El input no es un email válido!',
          }, {
            required: true, message: 'Por favor ingrese un email!',
          }],
        })(
          <Input />
        )}
      </FormItem>

        <FormItem
        label="Password"
        hasFeedback
      >
        {getFieldDecorator('password', {
          rules: [{
            required: true, message: 'Por favor ingrese la contraseña!',
          }, {
            validator: this.checkConfirm,
          }],
        })(
          <Input type="password" />
        )}
      </FormItem>

      <FormItem
        label="Confirm Password"
        hasFeedback
      >
        {getFieldDecorator('confirm', {
          rules: [{
            required: true, message: 'Por favor confirme su contraseña!',
          }, {
            validator: this.checkPassword,
          }],
        })(
          <Input type="password" onBlur={this.handleConfirmBlur} />
        )}
      </FormItem>
      </Form>
   );
  }

}
