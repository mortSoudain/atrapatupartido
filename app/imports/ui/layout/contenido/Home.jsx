import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import HeaderIntro from './header_intro';
import Eleccion from './eleccion';

export default class Home extends Component {
  render() {
    return(
      <div>
          <HeaderIntro/>
          <Eleccion/>
      </div>
    );
  }
}
