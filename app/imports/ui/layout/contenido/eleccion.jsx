import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Link, withRouter } from 'react-router-dom';

//import { Card, Col, Row } from 'antd';
import Card from "antd/lib/card";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

const { Meta } = Card;

class Eleccion extends Component {

  render() {
    return(
      <div className="gutter">
        <Row gutter={16}>
          <Col className="gutter-row" span={6}>
          </Col>
          <Col className="gutter-row" span={6}>
            <div className="gutter-box">
              <Link to="/match">
                <Card
                  style={{ width: 200 }}
                  cover={
                    <center>
                          <img className="homeImages" alt="match" src="https://i.imgur.com/OSANvkR.png" />
                    </center>
                  } >
                  <center>
                   <Meta title="Match" />
                  </center>
                </Card>
            </Link>
            </div>
          </Col>
          <Col className="gutter-row" span={6}>
            <div className="gutter-box">
              <Link to="/busqueda">
                <Card
                  style={{ width: 200 }}
                  cover={
                    <center>
                      <img className="homeImages" alt="busqueda" src="https://i.imgur.com/mqyowaY.png" />
                    </center>
                      } >
                  <center>
                    <Meta title="Busqueda" />
                  </center>
                </Card>
              </Link>
            </div>
          </Col>
          <Col className="gutter-row" span={6}>

          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(Eleccion);
