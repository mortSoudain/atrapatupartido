import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Input, InputNumber, DatePicker, Button } from 'antd';
const FormItem = Form.Item;

export class CrearPartido extends Component {

  handleSubmit = (e) => {
  e.preventDefault();
  this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
      console.log('Received values of form: ', values);
    }
  });
}

  guardarPartido(){

    console.log(this.props);

        const form = this.form;

        form.validateFields((err, values) => {
          if (err) {
            return;
          } else {
          console.log('Received values of form: ', values);

          //Meteor.call('visitas.insertar',values.uid,values.rut,values.nombre);

         }
        });

        form.resetFields();

  }


  render() {

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <div>

        <h1>Crear Partido</h1>
            <Form layout="vertical">
              <FormItem
                label={(
                  <span>
                    Jugadores Disponibles
                  </span>
                )}
              >
              {getFieldDecorator('jugadoresDisponibles', {
                rules: [{ type: 'number', required: true, message: 'Por favor selecciona una cantidad de jugadores disponibles' }],
              })(
                <InputNumber />
              )}
            </FormItem>

              <FormItem
              label={(
                <span>
                  Direccion
                </span>
              )}
            >
              {getFieldDecorator('direccion', {
                rules: [{ required: true, message: 'Por favor ingresa una direccion!', whitespace: true }],
              })(
                <Input />
              )}
            </FormItem>

            <FormItem
              label="Fecha y Hora"
            >
              {getFieldDecorator('fecha', {
                rules: [{ type: 'object', required: true, message: 'Por favor selecciona una hora' }],
              })(
                <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
              )}
            </FormItem>

            <FormItem
            label="E-mail"
          >
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'El input no es un email válido!',
              }, {
                required: true, message: 'Por favor ingrese un email!',
              }],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            label="Valor"
          >
            {getFieldDecorator('valor', {
              rules: [{ type: 'number', required: true, message: 'Introduce un valor por persona' }],
            })(
            <InputNumber
                 formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                 parser={value => value.replace(/\$\s?|(,*)/g, '')}
               />
          )}
        </FormItem>

        <Button onClick={this.handleSubmit} type="primary">OK</Button>

        </Form>
        </div>
    );
  }
}

export default CrearPartidoConcrete =  Form.create()(CrearPartido);
