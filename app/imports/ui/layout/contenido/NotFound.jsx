import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Button } from 'antd';

import { Link, Redirect } from 'react-router-dom';


export default class NotFound extends Component {

  state = {
    redirect: false,
  };

  handleOnClick = () => {
    this.setState({
      redirect: true,
    });
  };


  render() {

    if (this.state.redirect) {
      return <Redirect push to="/" />;
    }

    return (
      <div>
        <center>
          <h1 style={{ fontSize: 150, color: 'orange' }}> <i className="fa fa-question-circle"/> </h1>
          <h1>Lo Sentimos...</h1>
          <h2>La página que buscas no existe</h2>
          <Button type="primary" icon="rollback" onClick={this.handleOnClick}>
            <span>Volver</span>
          </Button>
        </center>
      </div>
    );
  }
}
