import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { withRouter } from 'react-router-dom';

//import { Icon, Row, Col  } from 'antd';
import Icon from "antd/lib/icon";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

class HeaderIntro extends Component {

  render() {
    return (
      <Row>
        <Col span={24}>
            <nav className="nav navbar-default">
              <div className="headerIntro">
                <img className="homeHeader" alt="homeHeader" src="https://i.imgur.com/9olosSO.png" />
              </div>
            </nav>

        </Col>
      </Row>
    );
  }
}

export default withRouter(HeaderIntro);
