import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { withRouter, Switch, Route } from 'react-router-dom';

import { Menu } from 'antd';
const SubMenu = Menu.SubMenu;

export default class SidebarMenu extends Component {

  constructor() {
     super();
     this.state = {
       selectedKeys: []
     }
   }


    static contextTypes = {
      router: PropTypes.object.isRequired
    }

 componentWillReceiveProps(nextProps, nextContext){
   this.setState({ selectedKeys: [nextContext.router.route.location.pathname] });
 }

 componentDidMount() {
   this.setState({ selectedKeys: [this.context.router.route.location.pathname] });
 }

  render() {
    return (
      <Menu
          theme="dark"
          defaultSelectedKeys={['1']}
          selectedKeys={this.state.selectedKeys}
          mode="inline"
        >
          <Menu.Item key="/">
              <span className="nav-text">
                <i className="fa fa-home"/> Inicio
              </span>
              <Link to='/'/>
          </Menu.Item>

        </Menu>
    );
  }
}
