import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

import { withTracker } from 'meteor/react-meteor-data';

import { Menu, Icon, Badge } from 'antd';
import { Row, Col } from 'antd';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

// NavbarMenu component - represents the whole app
export class NavbarMenu extends Component {

  constructor() {
     super();
     this.state = {
       selectedKeys: []
     }
   }

 static contextTypes = {
   router: PropTypes.object.isRequired
 }

 componentWillReceiveProps(nextProps, nextContext){
   this.setState({ selectedKeys: [nextContext.router.route.location.pathname] });
 }

 componentDidMount() {
   this.setState({ selectedKeys: [this.context.router.route.location.pathname] });
 }

  handleClick = (e) => {
    if(e.key==='/cerrarsesion'){
      Meteor.logout();
    }
  }

  render() {
    return (
      <Row>
        <Col span={8}>
        </Col>

        <Col span={16}>
          <Row type="flex" justify="end">
            <Col>

          <Menu
            onClick={this.handleClick}
            selectedKeys={this.state.selectedKeys}
            mode="horizontal">

              <SubMenu
                  title={<span><Icon type="user" style={{ fontSize: 14 }} /> Usuario</span>}
                  style={{ left: '50px'}}
                  >

                  <MenuItemGroup title={this.props.nombreUsuarioActual}>

                    <Menu.Item key="/usuarios">
                        <span className="nav-text">
                            <i className="fa fa-user-o"/> Ver Usuarios
                        </span>
                      <Link to='/usuarios'/>
                    </Menu.Item>
                    <Menu.Item key="/miperfil">
                        <span className="nav-text">
                            <i className="fa fa-user"/> Mi Perfil
                        </span>
                      <Link to='/miperfil'/>
                    </Menu.Item>
                    <Menu.Item key="/cerrarsesion">
                        <span className="nav-text">
                            <i className="fa fa-sign-out"/> Cerrar Sesion
                        </span>
                      <Link to='/'/>
                    </Menu.Item>
                  </MenuItemGroup>
                </SubMenu>

            </Menu>
            </Col>
          </Row>
          </Col>
        </Row>
     );
  }
}

export default withTracker(() => {
  return {
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
})(NavbarMenu);
